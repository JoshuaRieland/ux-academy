<?php
require_once('vendor/autoload.php');

$stripe = array(
  "secret_key"      => "SET_SECRET_KEY",
  "publishable_key" => "SET_PUBLISHABLE_KEY"
);

\Stripe\Stripe::setApiKey($stripe['secret_key']);
?>