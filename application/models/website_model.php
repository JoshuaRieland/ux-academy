<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Website_model extends CI_Model {

 	public function add_student_applicant($resume, $application){
 		$query = 'INSERT INTO student_applicants (first_name, last_name, email, phone_number, skypeID, portfolio_url, bootcamp ,why_ux_design, why_ux_academy, resume, created_at) VALUES (?,?,?,?,?,?,?,?,?,?, NOW())';
 		$this->db->query($query, array( 'first_name' => $application['first_name'], 
 										'last_name' => $application['last_name'], 
 										'email' => $application['email'], 
 										'phone_number' => $application['phone_number'], 
 										'skypeID' => $application['skypeID'],
 										'portfolio_url' => $application['portfolio_url'],
 										'bootcamp' => $application['bootcamp'],
 										'why_ux_design' => $application['why_ux_design'],
 										'why_ux_academy' => $application['why_ux_academy'],
 										'resume' => $resume
 										));
 		return;
 	}
           
        public function stay_informed($email){
 		$query = 'INSERT INTO stay_informed (email, created_at) VALUES (?, NOW())';
 		$accepted = $this->db->query($query, array('email' => $email));
 		return $accepted;
 	}

 	public function learn_ux_design($post){
 		$query = 'INSERT INTO learn_ux_design_cta (full_name, email, phone, created_at) VALUES (?,?,?, NOW())';
 		$accepted = $this->db->query($query, array('full_name' => $post['full_name'], 'email' => $post['email'], 'phone' => $post['phone']));
 		return $accepted;
 	}

 	public function ask_question($post){
 		$query = 'INSERT INTO question_comment (name, email, content, created_at) VALUES (?,?,?, NOW())';
 		$accepted = $this->db->query($query, array('name' => $post['name'], 'email' => $post['email'], 'content' => $post['content']));
 		return $accepted;
 	}

}