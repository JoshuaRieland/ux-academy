<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));

        }

        public function do_upload()
        {

                $config['upload_path']          = './resumes/';
                $config['allowed_types']        = 'zip|pdf|doc|docx|rtf|txt';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                        $application = $this->input->post();
                        $resume = 'No-Resume';

                       	$this->website_model->add_student_applicant($resume, $application);  
                       
                        $this->load->view('thank-you');
                }
                else
                {
                	$application = $this->input->post();
                        $data = array('upload_data' => $this->upload->data());
                        $resume = $data['upload_data']['file_name'];
                       
                       	$this->website_model->add_student_applicant($resume, $application);  
                       
                        $this->load->view('thank-you');
                }
        }

   


       
}
?>