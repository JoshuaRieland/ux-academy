<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	  {
	    parent::__construct();
	    /* Load the libraries and helpers */
	    $this->load->library('form_validation');
	    $this->load->library('session');
	    $this->load->helper(array('form', 'url', 'captcha'));
	  }

	public function index(){
		$this->load->view('index');
	}

	public function faq(){
		$this->load->view('faq');
	}

	public function contact_us(){

	      /* Setup vals to pass into the create_captcha function */
			$vals = array(
				'img_path' => './captcha/',
				'img_url' => './captcha/',
				'img_width' => '150',
				'img_height' => 50,
				'expiration' => 7200,
			);
	      	
	      	/* Generate the captcha */
	      	$captcha = create_captcha($vals);
	      
	      	/* Store the captcha value (or 'word') in a session to retrieve later */
	      	$this->session->set_userdata('captchaWord', $captcha['word']);
	      
	      	/* Load the captcha view containing the form (located under the 'views' folder) */
	      	$this->load->view('contact_us', $captcha);
	}

	public function stay_informed(){
		$this->load->helper('email');
		$email = $this->input->post('email');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'josh@uxacademy.io',
		    'smtp_pass' => 'changeme',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		// Set to, from, message, etc.
		$this->email->initialize($config);

        $this->email->from('info@uxacademy.io', 'UX Academy');
        $this->email->to($email); 

        $this->email->subject('Learn UX Design Today!');
        $this->email->message('Thank you for showing interest in UX Academy. We are happy to give you more information and help further your design career.'); 

		$result = $this->email->send();

		$accepted = $this->website_model->stay_informed($email);
		if($accepted){
			echo json_encode('Thank you for your interest in UX Academy.');
		}
		else{
			echo json_encode('There was an error in the submission, pleae try again later');
		}

	}

	public function learn_ux_design(){
		$post = $this->input->post();
		$accepted = $this->website_model->learn_ux_design($post);
		$this->load->helper('email');
		$email = $this->input->post('email');

		$config = Array(
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'josh@uxacademy.io',
		    'smtp_pass' => 'changeme',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		// Set to, from, message, etc.
		$this->email->initialize($config);

        $this->email->from('admissions@uxacademy.io', 'UX Academy Admissions');
        $this->email->to($email); 

        $this->email->subject('We recieved your application.');
        $this->email->message('Thank you for showing interest in UX Academy and taking the first step towards expanding your UX Design career.'); 

		$result = $this->email->send();
		
		if($accepted){
			if($result){
				echo json_encode("Thank you for your interest in UX Academy! You will receive an email with next steps in the registration process.");

			}
			else{
				echo json_encode("Thank you for your interest in UX Academy! There was an error sending a response email to the supplied email. Please contact us at admissions@uxacademy.io for further registration instructions.");					
			}

		}
		else{
			echo json_encode("There was an error in the submission, please try again in a few minutes.");
		}
        
	}

	public function ask_question(){
		$post = $this->input->post();
		$accepted = $this->website_model->ask_question($post);
		if($accepted){
			echo json_encode("Thank you for submitting your question or concern. Our team will respond to you as soon as possible.");
		}
		else{
			echo json_encode("There was an error in the submission. Please try submitting your question again in a few minutes. Thank you, UX Academy.");
		}
	}

	public function email_question(){
		// var_dump($this->input->post());
		// var_dump($_SESSION['captchaWord']);
		// die();
		session_start();

		if( strtoupper($_POST['captcha']) == strtoupper($_SESSION['captchaWord']) ){
			
			$posted = $this->input->post();

			$this->load->helper('email');
			$email = $this->input->post('email');

			$config = Array(
			    'protocol' => 'smtp',
			    'smtp_host' => 'ssl://smtp.googlemail.com',
			    'smtp_port' => 465,
			    'smtp_user' => 'uxahelpteam@gmail.com',
			    'smtp_pass' => 'here2help',
			    'mailtype'  => 'html', 
			    'charset'   => 'iso-8859-1'
			);
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");

			// Set to, from, message, etc.
			$this->email->initialize($config);

	        $this->email->from( $email, $_POST['name']);
	        $this->email->to('uxahelpteam@gmail.com'); 
	        if( isset($_POST['copy']) && $_POST['copy'] == 'on' ){
	        	$this->email->cc($email);
	        }
	        $this->email->subject($posted['subject']);
	        // $this->email->message(
	        // 	'<p style="font-size: 14px;">From:    '.$_POST['name'].'      '.$email.'</p>'.
	        // 	'<p style="font-size: 14px;">Subject: '.$_POST['subject'].'</p>'.
	        // 	'<p style="font-size: 12px;">Message: '.$posted['message']
	        // ); 
	        $this->email->message($_POST['message']);
			$result = $this->email->send();

			// if( $_POST['copy'] == 'on' )
			// {
			// 	$this->email->from('contact-us@uxacademy.io', 'UX Academy');
	  //       	$this->email->to($email); 

	  //       	$this->email->subject($posted['subject']);
	  //       	$this->email->message($posted['message']); 

			// 	$second_result = $this->email->send();
			// }

			// $accepted = $this->website_model->stay_informed($email);
			if($result){
				echo json_encode('We have received your inquiry and will address your questions/concern as soon as we can.');
				// if($second_result){
				// 	echo json_encode('A copy email has been sent to ')
				// }
			}
			else{
				echo json_encode('There was an error in the submission, pleae try again later');
			}
		}
	}

	public function payments(){
		$this->load->view('payments');
	}

	public function charge(){
	  	require_once('./config.php');
  		$token  = $_POST['stripeToken'];
  		$customer = \Stripe\Customer::create(array(
      		'email' => $_POST['stripeEmail'],
      		'card'  => $token
  		));
  		$charge = \Stripe\Charge::create(array(
      		'customer' => $customer->id,
      		'amount'   => 299500,
      		'currency' => 'usd'
  		));
  		$this->load->view('payment-confirmed', array('charge' => $charge));
	}

	public function charge_split(){
	  	require_once('./config.php');
  		$token  = $_POST['stripeToken'];
  		$customer = \Stripe\Customer::create(array(
      		'email' => $_POST['stripeEmail'],
      		'card'  => $token
  		));
  		$charge = \Stripe\Charge::create(array(
      		'customer' => $customer->id,
      		'amount'   => 299500,
      		'currency' => 'usd'
  		));
  		$this->load->view('payment-confirmed', array('charge' => $charge));
	}

	public function payment_confirmed(){
		$this->load->view('payment-confirmed');
	}
}

