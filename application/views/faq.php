<!DOCTYPE html>
<html class="full" lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Frequently Asked Questions | UX Academy</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,300,600,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
  $(document).on('submit', 'form.learnUX', function(){
   $.post(
        $(this).attr('action'),
        $(this).serialize(),
        function(returned_data){
          console.log(returned_data);
          $('form').hide();
          $('div.message').append(
            // "<p class='message'>Thank you for interest in UX Academy. We will email you shortly.</p>"
            "<h5>"+returned_data+"</h5>"
            )
        },
        "json"
      )   
      return false; 
  });
  $(document).on('submit', 'form#ask-question', function(){
   $.post(
        $(this).attr('action'),
        $(this).serialize(),
        function(returned_data){
          console.log(returned_data);
          $('form').hide();
          $('div.question_message').append(
            "<h5>"+returned_data+"</h5>"
            )
          $('div.modal-content').append(
            "<div class='modal-footer'>"+"<button type='button' class='btn btn-default' data-dismiss='modal'>"+"Close"+"</button>"+"</div>"
            )
        },
        "json"
      )   
      return false; 
  });

   // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 2000, 'easeInOutExpo');
            event.preventDefault();
        });
    });
</script>

</head>

<body id='inside-pages' data-spy="scroll" data-target=".navbar-static-top">
    
    <div id='faq-question-box' data-toggle="modal" data-target="#question-modal" class='hidden-xs'>
      <span>?</span>
    </div>
    <div id='faq-question-box-small' data-toggle="modal" data-target="#question-modal" class='hidden-sm hidden-md hidden-lg hidden-xl'>
      <span>?</span>
    </div>
    <div class="modal fade" id="question-modal" tabindex="-1" role="dialog" aria-labelledby="question_modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="question_modal">Have a question not covered or want a more indepth response? Please let us know and we will gladly address your question or concern.</h4>
          </div>
          <div class='question_message'></div>
          <form id='ask-question' method='POST' action='ask-question'>
            <div class="modal-body">
               <div class="form-group">
                <label for="full_name">Your Name</label>
                <input type="full_name" name='name' class="form-control" id="full_name" placeholder="">
              </div>
              <div class="form-group">
                <label for="email">Your Email Address</label>
                <input type="email" name='email' class="form-control" id="email" placeholder="">
              </div>
              <div class="form-group">
                <label for="content">Question/Comment/Concern</label>
                <textarea class="form-control" name='content' id="content" placeholder="" style='min-height: 150px;'></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-default pull-left">Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/img/ux-academy-logo-2.png" alt='ux academy logo'>
                    <!-- <p class='logo-text'><strong>UX</strong>ACADEMY</p> -->
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ">

                    <li>
                        <a href="#learn-ux-cta" class='page-scroll'><strong>Apply Now</strong></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- start hero -->
    <section id="intro">
        <div class="row">
            <div class="course-hero"  id='faq_bg'>
                <div class="container">

                    <!-- hero content section -->
                    <div class="hero-content">
                        <h2 class='hero-heading'>Frequently Asked Questions</h2>
                        <h1 class="hero-small">Find answers or send us an email<br/> We're here to help.</h1>
                    </div>
                    <!-- hero content end -->

                </div>
            </div>
        </div>
      
    </section>
    <!-- end hero -->
    


    <!-- course overview begin   -->
    <section id="course-section">

       <div class='faq'>
        <div class='container'>
          <!--START: FAQ Pricing -->
          <div class='row'>       
            <div class='faq-pricing'>
                <header>
                  <h3 class='faq'>Pricing</h3>
                </header>
                <hr>
              
                <!--START: FAQ Pricing Left Column -->
                <div class='col-xs-12 col-sm-6'>
                  <div class='col-xs-12'>
                    <h4>How much does UX Academy cost?</h4>

                    <p class='faq-topic-answer'>The total cost of the course is $2995.</p>


                  </div>
                   <div class='col-xs-12'>
                    <h4>Are payment plans available?</h4>
                    <p class='faq-topic-answer'>Yes. You can choose to pay the total amount ($2995) upfont or pay over 5 installments of $399 + $1000 enrollment fee. </p>
                    <ul class='faq-no-bullet-list'>
                      <li><span class='highlight-bold'>Payment Plan</span></li>
                      <li><span class='highlight-bold'>$1000</span>&nbsp;- due upon enrollment</li>
                      <li><span class='highlight-bold'>$399</span>&nbsp;- 5 Monthly Installments</li>
                    </ul>
                    <p class='faq-topic-note'>**Payment plans are not available if you pay with a check or through Paypal.</p>
                  </div>
                </div>
                <!--END: FAQ Pricing Left Column -->

                <!--START: FAQ Pricing Right Column -->
                <div class='col-xs-12 col-sm-6'>
                 
                  <div class='col-xs-12'>
                    <h4>What is your refund policy?</h4>
                    <p class='faq-topic-answer'>UX Academy's course is results-driven. If you feel as though UX Academy is not the right fit for you after all, you can withdraw from the course at anytime. Withdraw in the first week of your start date, and receive a full refund. If you withdraw later, get a refund based of the number of days you have been in the program, minus a non-refundable cancelation fee of $1000.</p>
                  </div>
                   <div class='col-xs-12'>
                    <h4>What does UX Academy accept for payment?</h4>
                    <p class='faq-topic-answer'>We accept all credit & debit cards, checks, and Paypal. If you are paying with a check or Paypal, you must pay the total amount upfront.</p>
                  </div>
                </div>
                <!--END: FAQ Pricing Right Column -->

            </div>
          </div>
          <!-- END: FAQ Pricing -->

          <!--START: FAQ Curriculum -->
          <div class='row'>       
            <div class='faq-Curriculum'>
                <header>
                  <h3 class='faq'>Curriculum</h3>
                </header>
                <hr>
              
                <!--START: FAQ Curriculum Left Column -->
                <div class='col-xs-12 col-sm-6'>
                  <div class='col-xs-12'>
                    <h4>What am I going to learn at UX Academy?</h4>
                    <p class='faq-topic-answer'>Our <a href="https://www.uxacademy.io"> User Experience Design</a> course is comprised of design concepts (UI & UX) for web and mobile applications. The course focuses on user research, usability testing, and learning industry standard tools like Photoshop, Illustrator, Sketch, and InVision. </p>
                    <p class='faq-topic-answer'>If you're looking to focus on user experience and design without digging into code, then our course is for you.</p>
                  </div>
                   <div class='col-xs-12'>
                    <h4>What if I can't keep up with curriculum or I get sick?</h4>
                    <p class='faq-topic-answer'>UX Academy is a big commitment, and we expect for you to put in the hours to fully learn the course material. We do understand that life just happens sometimes. If you happen to get sick or you begin to notice that you are falling behind in the course, contact your Student Advisor immediately so we can make a plan that necessary for your needs. </p>
                  </div>
                </div>
                <!--END: FAQ Curriculum Left Column -->

                <!--START: FAQ Curriculum Right Column -->
                <div class='col-xs-12 col-sm-6'>
                 
                  <div class='col-xs-12'>
                    <h4>What is the time commitment for the course?</h4>
                    <p class='faq-topic-answer'>We have designed UX Academy to be an immersive course that can fit into your life's schedule. Yes, that means that you don't have to quit your job or relocate to attend our course. Choose a pace that works best with your schedule:</p>
                    <ul class='faq-blue-bullet-list'>
                      <li>12 weeks | 35-40 hours per week | 3 mentor meetings per week</li>
                      <li>24 weeks | 20-25 hours per week | 2 mentor meetings per week</li>
                      <li>36 weeks | 10-15 hours per week | 1 mentor meetings per week</li>
                    </ul>
                  </div>
                   <div class='col-xs-12'>
                    <h4>Is there any required prep work?</h4>
                    <p class='faq-topic-answer'>Yes. Once you enroll, we will provide you with prepwork to complete prior to your course start date. The amount of time needed to spend on prep work varies on your experience level. That said, we recommend you give yourself 50-100 hours to spend on prep work before your course start date.</p>
                  </div>
                </div>
                <!--END: FAQ Curriculum Right Column -->

            </div>
          </div>
          <!-- END: FAQ Curriculum -->

          <!--START: FAQ Admissions -->
          <div class='row'>       
            <div class='faq-Admissions'>
                <header>
                  <h3 class='faq'>Admissions</h3>
                </header>
                <hr>
              
                <!--START: FAQ Admissions Left Column -->
                <div class='col-xs-12 col-sm-6'>
                  <div class='col-xs-12'>
                    <h4>Do I need to have any design experience to attend UX Academy?</h4>
                    <p class='faq-topic-answer'>No. You do not need any experience to enroll in UX Academy. We have designed our prep work for beginners so you can start with a solid design foundation before your course starts.</p>
                  </div>
                   <div class='col-xs-12'>
                    <h4>What is UX Academy looking for in applicants?</h4>
                    <p class='faq-topic-answer'><span class='highlight-bold'>Passion:</span> We believe that we can change the world by creating a better experience in people’s everyday lives and we look for people who share this same belief.</p>
                    <p class='faq-topic-answer'><span class='highlight-bold'>Committed:</span> When you're learning an entirely new skillset, it’s not easy. It takes time, practice, repetition, patience, and commitment. Stay committed to the process and you will be proud at what you’ve accomplished in such a short time.</p>
                    <p class='faq-topic-answer'><span class='highlight-bold'>Positive:</span> As mentioned above, learning a new skillset requires patience. In the beginning, things can be frustrating and confusing because everything is so new. </p>
                  </div>
                  <div class='col-xs-12'>
                    <h4>What’s the deadline for enrolling?</h4>
                    <p class='faq-topic-answer'>Course registration will close 3 weeks before your start date. This will give you enough time to complete the pre-course material.</p>
                  </div>
                </div>
                <!--END: FAQ Admissions Left Column -->

                <!--START: FAQ Admissions Right Column -->
                <div class='col-xs-12 col-sm-6'>
                 
                  <div class='col-xs-12'>
                    <h4>What happens after I apply?</h4>
                    <p class='faq-topic-answer'>Once you apply, a Student Advisor will contact you within 2-3 business days to setup a phone meeting. This meeting is so we can better understand your career goals and your experience level, while providing you with the opportunity to ask any questions you may have.</p>
                  </div>
                   <div class='col-xs-12'>
                    <h4>Can I defer or change my cohort date after I enroll?</h4>
                    <p class='faq-topic-answer'>Yes, we understand that things in life happen. If you need to change your enrollment date, contact our admissions department by e-mail at: <a href="mailto:admissions@uxacademy.io" target="_top">admissions@uxacademy.io</a></p>
                  </div>
                </div>
                <!--END: FAQ Admissions Right Column -->

            </div>
          </div>
          <!-- END: FAQ Admissions -->

          <!--START: FAQ Job Support -->
          <div class='row'>       
            <div class='faq-Job Support'>
                <header>
                  <h3 class='faq'>Job Support</h3>
                </header>
                <hr>
              
                <!--START: FAQ Job Support Left Column -->
                <div class='col-xs-12 col-sm-6'>
                  <div class='col-xs-12'>
                    <h4>Does UX Academy offer career support?</h4>
                    <p class='faq-topic-answer'>Yes. When you complete the course you will have two options:</p> 
                    <p><strong> Job Prep</strong> -work on our job preparation section where you will clean up your resume, conduct mock interviews with your mentor, and prepare you for your first job.</p>
                    <p><strong> Client Project Center</strong> - At the end of the course you will wiork with your student advisor to set you up with real client projects so you can gain immediate experience and start making an income. For more information regarding our client project center, contact <href="mailto:admissions@uxacademy.io" target="_top"> careers@uxacademy.io</a></p>
                  </div>
                  <div class='col-xs-12'>
                    <h4>Are you guaranteeing me a job?</h4>
                    <p class='faq-topic-answer'>Our client projects are available to students who successfully graduate from the program. By successfully completing the course we mean that you must complete all of the projects and your mentor must recommend you. At this time, client project will become available to you.</p>
                  </div>
                   

                </div>
                <!--END: FAQ Job Support Left Column -->

                <!--START: FAQ Job Support Right Column -->
                <div class='col-xs-12 col-sm-6'>
                 
                  <!-- <div class='col-xs-12'>
                    <h4>Can I defer or change my cohort date after I enroll?</h4>
                    <p class='faq-topic-answer'>Yes, we understand that things in life happen. If you need to change your enrollment date, contact our admissions department by e-mail at: admissions@uxacademy.io</p>
                  </div>
                   <div class='col-xs-12'>
                    <h4>Will UX Academy take a percentage of my income when I get a job?</h4>
                    <p class='faq-topic-answer'>Yes, we understand that things in life happen. If you need to change your enrollment date, contact our admissions department by e-mail at: admissions@uxacademy.io</p>
                  </div> -->

                  <div class='col-xs-12'>
                    <h4>How long will take for me to find a job?</h4>
                    <p class='faq-topic-answer'>The amount of time for you to find a job varies from student to student. Typically the process takes anywhere from 6 weeks to six months after the program. UX Academy does everything we can to prepare you for finding a job, but this ultimately depends on you.</p>
                  </div>

                </div>
                <!--END: FAQ Job Support Right Column -->

            </div>
          </div>
          <!-- END: FAQ Job Support -->

          <!--START: FAQ Mentorship -->
          <div class='row'>       
            <div class='faq-Mentorship'>
                <header>
                  <h3 class='faq'>Mentorship</h3>
                </header>
                <hr>
              
                <!--START: FAQ Mentorship Left Column -->
                <div class='col-xs-12 col-sm-6'>
                  <div class='col-xs-12'>
                    <h4>How does mentoring work exactly?</h4>
                    <p class='faq-topic-answer'>A week before the course starts, you will be able to choose your mentor. Each week you will have weekly meetings with your mentor to provide feedback on assignments, projects, and help you prepare for your first job. </p>
                    <p class='faq-topic-meeting-title'>Time, Meetings, & Hours Required</p>
                    <p class='faq-topic-meeting-list'>12 weeks | 3 Mentor Meetings per week | 40 hours per week</p>
                    <p class='faq-topic-meeting-list'>24 weeks | 2 Mentor Meetings per week | 25 hours per week</p>
                    <p class='faq-topic-meeting-list'>36 weeks | 1 Mentor Meetings per week | 15 hours per week</p>
                  </div>
                   <div class='col-xs-12'>
                    <h4>How do I choose a mentor?</h4>
                    <p class='faq-topic-answer'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</p>
                  </div>
                </div>
                <!--END: FAQ Mentorship Left Column -->

                <!--START: FAQ Mentorship Right Column -->
                <div class='col-xs-12 col-sm-6'>
                 
                  <div class='col-xs-12'>
                    <h4>What if I want to switch mentors?</h4>
                    <p class='faq-topic-answer'>If you feel as though your mentor isn't the right fit for you, contact your Student Advisor to request a mentor change.</p>
                  </div>
                </div>
                <!--END: FAQ Mentorship Right Column -->

            </div>
          </div>
          <!-- END: FAQ Mentorship -->

        </div>
       </div>

        <!-- Start CTA Learn UX Design -->
        
          <div class='row' id='learn-ux-cta'>
            <div class="container">
              <a name="apply"></a>
            <h3>Next course start date is on <span>October 19th.</span> Apply Now.</h3>
            <form class='learnUX' method='POST' action='learn-ux-design'>
              
              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-user'></i>
                <input type='text' class='form-control' name='full_name' placeholder='Your Full Name' required>
              </div>            

              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-envelope'></i>
                <input type='email' class='form-control' name='email' placeholder='Email Address' required>
              </div>

              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-phone'></i>
                <input type='tel' class='form-control' name='phone' placeholder='Phone Number' required>
              </div>

              
              <button type='submit' id='learn-ux-submit'>LEARN UX DESIGN</button>
              

            </form>

            <div class="message"></div>
            <p id='privacy_guarantee'>We guarantee 100% privacy. Your information will not be shared.</p>
          </div>
        </div>
        <!-- End CTA Learn UX Design -->

        <footer id="footer">
          <div class="container">
            <!-- <div class="container"> -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="footer-nav">
                          <div class="col-xs-6 col-sm-4">
                            <div class="social-icons">
                              <a href="https://www.facebook.com/uxacad" target='_'><i class="fa fa-facebook"></i></a>
                              <a href="https://twitter.com/AcademyUX" target'_'><i class="fa fa-twitter"></i></a>
                              
                            </div>
                          </div>
                             <!-- <ul class="footer-list pull-left">
                              <li> -->
                                   
                              <!-- </li>
                            </ul> -->
                          <div class="col-md-4  visible-md visible-lg">  
                            <span class='copyright visible-sm visible-md visible-lg'>&copy 2015 UX Academy, LLC.  All Rights Reserved.</span>
                          </div>
                          <div class="col-xs-6 col-sm-4">
                              <ul class="footer-list pull-right">
                                  <li>
                                      <span class="questions"> Questions? </span><a href="mailto:someone@example.com">info@uxacademy.io</a>
                                  </li>
                              </ul>
                          </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
            </div>
        </footer>
     

     
        <!-- jQuery -->
        <script src="/assets/js/jquery.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/assets/js/jquery.easing.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/assets/js/bootstrap.min.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-64642522-1', 'auto');
          ga('send', 'pageview');

        </script>

        <script type="text/javascript">
           $(".timer").countdown("2015/07/30", function(event) {
             $('.days').text(event.strftime('%D'));
             $('.hours').text(event.strftime('%H'));
             $('.minutes').text(event.strftime('%M'));
             $('.seconds').text(event.strftime('%S'));
           });
         </script>

</body>

</html>