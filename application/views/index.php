<!DOCTYPE html>
<html class="full" lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Learn to become a UX designer by working through a series of hands on projects while receiving mentorship from experienced designers.">

    <title>Become a UX Designer in 12 weeks | UX Academy</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="/assets/img/uxa-favicon.png" type="image/png" />
    <!-- Custom CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,300,600,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/process_tree.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
  
    // AJAX Application Form
    $(document).on('submit', 'form.learnUX', function(){
        $.post(
            $(this).attr('action'),
            $(this).serialize(),
            function(returned_data){
                console.log(returned_data);
                $('form.learnUX').hide();
                $('div.message').append(
                    "<h5>"+returned_data+"</h5>"
                )
            },
            "json"
        )   
        return false; 
    });

    // AJAX Stay Informed Form
    $(document).on('submit', 'form.stay_informed', function(){

        $.post(
            $(this).attr('action'),
            $(this).serialize(),
            function(returned_data){
                console.log(returned_data);
                $('form.stay_informed').hide();
                $('div.informed_message').append(
                    "<h5>"+returned_data+"</h5>"
                )
            },
            "json"
        )   
        return false; 
    });

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 2000, 'easeInOutExpo');
            event.preventDefault();
        });
    });
   
    // AJAX Loading Spinner
    $(document).ready(function(){
    var $loading = $('.loadingDiv').hide();
    $(document)
      .ajaxStart(function () {
        $loading.show();
      })
      .ajaxStop(function () {
        $loading.hide();
      });
    })


</script>
</head>

<body id='inside-pages' data-spy="scroll" data-target=".navbar-static-top">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><img src="/assets/img/ux-academy-logo-2.png" alt='ux academy logo'>
                    <!-- <p class='logo-text'><strong>UX</strong>ACADEMY</p> -->
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ">
                    <li>
                        <a href="#learn-ux-cta" class='page-scroll'><strong>Apply Now</strong></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <section id="intro">
        <div class="hero visible-sm visible-md visible-lg">
            <div class="container">
                <!-- hero content section -->
                <div class="hero-content">
                    <h2 class='hero-heading'> Design a new career</h2>
                    <h1 class="hero-small"><strong>Become a UX Designer</strong> in 12, 24, or 36 weeks <br /> with our online, project-based course.</h1>
                    <div class="ctas">
                       <form class='stay_informed' action='stay-informed' method='POST'>
                            <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                                <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                            </div>
                            <label for='informed_email'>Courses Begin Every 6 Weeks</label>
                            <input type='email' name='email' placeholder='Enter Your Email' id='informed_email' class='informed_email' required>
                            <button type='submit' class='informed_btn'>Stay Informed</button>
                        </form> 
                        <div class='informed_message'></div>
                        <!-- <a href="/ux-design-course"><button type="button" class="btn btn-primary">Learn more about <br /> the course</button></a> -->
                    </div>
                    
                </div>
                <!-- hero content end -->
            </div>
        </div>
        <!-- will not upload photo as extra small screen res -->
                        
        <div class="hero-mobile visible-xs">
            <div class="hero-content">
                <h2 class='hero-heading'> Design a new career</h2>
                    <h1 class="hero-small"><strong>Become a UX Designer</strong> in 12, 24, or 36 weeks <br /> with our online, project-based course.</h1>
                    <div class="ctas">
                        <div id='loadingDiv' style='width: 50px; height: 50px; left: 200px; right: 200px; background-color: red; z-index: 9999;'></div>
                       <form class='stay_informed visible-lg visible-md visible-sm' action='/stay-informed' method='POST'>
                            <label for='informed_email'>Courses Begin Every 6 Weeks</label>
                            <input type='email' name='email' placeholder='Enter Your Email' id='informed_email' class='informed_email' required>
                            <button type='submit' class='informed_btn'>Stay Informed</button>
                        </form> 
                         <div class='informed_message'></div>
                        <!-- <button type="button" class="btn btn-primary btn-hero">Get a course <br />syllabus now!</button> -->
                        <!-- <a href="/ux-design-course"><button type="button" class="btn btn-default">Learn more about <br /> the course</button></a> -->
                    </div>
                    
                </div>
            </div>
    </section>


    <section id='course-section'>

        <!-- start highlights -->
        <div class="highlights">
        
            <div class="row">
                <div class="container">
                    <div class="col-xs-12 col-sm-6">
                        <h3 class="heading">Course Highlights</h3>
                        <p>UX Academy is the first online learning course of its kind. Not only will you gain hands-on experience designing web and mobile applications, but we will also set you up with real client projects when you become a UX Academy alumni. Begin your journey to a new, lucrative career in UX Design by completing our project-based course.</p>
                        <ul class="items">
                    
                            <li><i class="fa fa-long-arrow-right"></i>Design production quality applications.</li>
                            <li><i class="fa fa-long-arrow-right"></i>Learn from experienced UX Designers.</li>
                            <li><i class="fa fa-long-arrow-right"></i>Gain exclusive access to our client project center</li>
                        </ul>
                        

                        <a href="#learn-ux-cta" class='page-scroll'><button type="button" class="btn btn-primary btn-lg">Get Started</button></a>
                        <!-- <a href="/course-application"><button type="button" class="btn btn-primary btn-lg">Request a Syllabus</button></a> -->
                    </div>

                    <div class="col-sm-6 visible-sm visible-md visible-lg">
                        <!-- <div class="uxa-platform">
                            <img src="/assets/img/skype.jpg" class='img-responsive top top2' alt='Mentor Call'>
                        </div> -->
                        <div class="uxa-platform">
                            <img src="http://placehold.it/575x355">
                            <!-- <img src="/assets/img/platform-4.jpg" class='img-responsive top' alt='learning platform'> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end highlights -->



        <!-- start what you'll learn -->
        <div class="students-learn">
            <!-- <div class="container">  -->
            <div class="row">
                <div class="container-stats ">
                <div class='container'>
                    <div class="col-xs-12 ">
                        
                        
                            <div class='col-xs-12 col-sm-4 left-stat'>
                                <p class='stat'><span>$</span>90,000</p>
                                <p class='stat-title'>Average Salary</p>
                                <p class='stat-source'>Source: <a href="http://www.glassdoor.com/Salaries/user-experience-designer-salary-SRCH_KO0,24.htm" target='_'>Glassdoor</a></p>
                            </div>
                            <div class='col-xs-12 col-sm-4 mid-stat'>
                                <p class='stat'><span><i class="fa fa-arrow-up"></i></span>18%</p>
                                <p class='stat-title'>Job Growth</p>
                                <p class='stat-source'>Source: <a href="http://money.cnn.com/gallery/pf/2015/01/27/best-jobs-2015/14.html" target='_'>CNN Money</a></p>
                            </div>
                            <div class='col-xs-12 col-sm-4 right-stat'>
                                <p class='stat'>30,000<span>+</span></p>
                                <p class='stat-title'>Jobs Available</p>
                                <p class='stat-source'>Source: <a href="http://www.simplyhired.com/search?q=ux+designer" target='_'>Simply Hired</a></p>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
            </div>
            <div class="what-you-learn">

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-sm-6 visible-md visible-lg ">
                        <img src="/assets/img/platform-4.jpg" class='img-responsive top' alt='learning platform'>
                        </div>

                        <div class="col-xs-12 col-md-6 youll-learn">
                            <h3 class="heading">What You'll Learn</h3>
                            <p>On the first day of class, begin working through a structured UX Design process as you design your first web and mobile (iPhone & Android) application. This process will teach you how to conduct in-depth user research, create a detailed strategy, design production quality applications and websites, and test your application for improvements.   </p> 
                            
                            <p> You will perform a competitor analysis, conduct user interviews and user surveys, create a market segmentation analysis, design personas and user flows, develop a content strategy, create high and low fidelity wireframes, perform user testing and design fully interactive prototypes.</p>
                            

                            <ul class="items">
                                <li><i class="fa fa-long-arrow-right"></i>Test your design assumptions on potential users.</li>
                                <li><i class="fa fa-long-arrow-right"></i>Learn how to meet user goals while meeting business goals.</li>
                                <li><i class="fa fa-long-arrow-right"></i>Design production quality web and mobile prototypes.</li>
                            </ul>
                            <a href="#learn-ux-cta" class='page-scroll'><button type="button" class="btn btn-primary btn-lg">Get Started</button></a>
                        </div>
                        </div>
                    </div>
                    
                    
                </div>

            </div>
        </div>  
            
        <!-- tools they will use -->
        <div class="row">
            <div class="tools-used visible-sm visible-md visible-lg">
                <div class="container">
                    <div class="col-xs-12 tools-logos">
                        <h5 class='tools'>People are changing the world with these tools and so can you</h5>
                        <div class="logos-wrapper">
                            <figure>
                                <img src="/assets/img/brands/user-testing-logo.png" alt='User Testing logo' title="User Testing">
                            </figure>
                            <figure>
                                <img src="/assets/img/brands/balsamiq-logo.jpg" alt='balsamiq logo' title="Balsamiq">
                            </figure>
                            <figure> 
                                <img src="/assets/img/brands/crazy-egg.jpg" alt='crazy egg logo' title="Crazy Egg">
                            </figure>
                            <figure>
                                <img src="/assets/img/brands/sketch-logo-small.png" alt='sketch logo' title="Sketch">
                            </figure>
                            <figure>
                                <img src="/assets/img/brands/photoshop-logo.png" alt='photoshop logo' title="Photshop">
                            </figure>
                            <figure>
                                <img src="/assets/img/brands/adobe-illustrator.png" alt='adobe illustrator logo' title="Adobe Illustrator">
                            </figure>
                            <figure>
                                <img src="/assets/img/brands/invision-logo-color.png" alt='Invision logo' title="Invision">
                            </figure>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <!-- course path -->
    <section class="learning-uxa">
        <div class="container process">
            <h2>Learning at UX Academy</h2>
            <p class="lead">
                UX Academy is exciting, challenging, and rewarding. After working through our curriculum, you will be armed with a portfolio of projects that will help you begin a successful career in user experience design.   
            </p>
            <hr>
            <div class="divide40"></div>
            <div class="process-step"> <span class="process-border"></span>
               


                <div class="step even">
                    <!-- <div class="icon-square"> <i class="fa fa-lightbulb-o"></i> </div> -->
                    <div class="icon-square"> <i class="fa fa-clock-o"></i> </div>
                    <h5>Learn on Your Time</h5>
                    <p>UX Academy is completely online, giving you the freedom to learn on your time and from anywhere in the world. Our course is flexible and designed to suit your needs. Choose a pace that works best with your schedule: </p>
                    <ul class="blue-points">
                        <li>12 Weeks | 3 Mentor Meetings Per Week | 40 Hours Per Week</li>
                        <li>24 Weeks | 2 Mentor Meetings Per Week | 25 Hours Per Week</li>
                        <li>36 Weeks | 1 Mentor Meetings Per Week | 15 Hours Per Week</li>
                    </ul>
                </div>

                <div class="step">
                    <div class="icon-square"> <i class="fa fa-user"></i> </div>
                    <h5>Select a Mentor</h5>
                    <p>Each of our mentors are experienced designers with at least 3 years of industry experience. Let us match you with a mentor who will:<br />

                    </p>
                    <ul class="blue-points">
                        <li>Help you stay on track and hold you accountable</li>
                        <li>Thoroughly explain core concepts</li>
                        <li>Review and provide feedback on assignments and projects</li>
                    </ul>
                    
                </div>
                <div class="step even">
                    <div class="icon-square"> <i class="fa fa-picture-o"></i> </div>
                    <h5>Build Your Portfolio</h5>
                    <p>Our course is designed so that you can gain hands-on experience while you build your portfolio. By the end of the course, you will have completed at least 3 production quality projects for your to land your next job. </p>
                </div>
                <div class="step">
                    <div class="icon-square"> <i class="fa fa-briefcase"></i></i> </div>
                    <h5>Job Prep & Career Support</h5>
                    <p>At the end of the course, work with your Course Advisor to receive  career support, advice, & feedback on your resume, portfolio, and job search. When you are ready, work with your course advisor to take on your first real client project.</p>
                </div>
            </div><!--steps-->
        </div>
    </div>
    <!-- course path end -->

    <!-- Start How Mentoring Works -->
    <section id="mentors">
        <div class='row'>
            <div class='container'>
                <div class='col-xs-12 col-sm-6 leftcolumn'>
                    <h3>How Mentoring Works</h3>
                    <p>A week before your course start date, you will choose your mentor and schedule weekly Skype meetings for design reviews. Your mentor will review your assignments & projects, bring clarity to core design concepts, and keep you on track throughout the course. </p>
                    <ul class="blue-points">
                        <li>Schedule weekly 30-minute Skype meetings</li>
                        <li>Receive feedback and critiques on assignments & projects</li>
                        <li>Prepare yourself for an interview by conducting mock interviews</li>
                    </ul>
                </div>
                <div class='col-xs-12 col-sm-6'>
                    <img src="/assets/img/skype.jpg" alt='Skype call with mentor' class='img-responsive'>
                </div>
            </div>
        </div>
    </section>
        <!-- End How Mentoring Works -->
    <section id='course-tuition'>   
        <!-- Start Course Tuition -->
        <div class='row'>
            <div class='container'>
                <h3>
                    <i class="fa fa-book"></i>Course <span class='plus'>+</span>
                    <i class="fa fa-comments"></i>Mentoring <span class='plus'>+</span>
                    <i class="fa fa-briefcase"></i>Support Pricing
                </h3>
                <p class='tuition-note'>For a limited time, get $250 off when you pay total amount upfront.</p>
                <div class='row'>
                    <div class='col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-2 col-lg-3 col-lg-offset-3 tuition-info'>
                        <p class='tuition-header'>ALL UPFRONT</h4>
                        <p class='tuition-price'>$2995</p>
                        <p class='tuition-price-note'>One-time payment</p>
                        <a href="#learn-ux-cta" class='page-scroll'><button>Get Started</button></a>
                        <ul class="key-points">
                            <li>Job Preparation</li>
                            <li>Project-based Curriculum</li>
                            <li>1-on-1 Mentorship</li>
                            <li>Project Center Access</li>
                        </ul>
                    </div>
                    <div class='col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-0 col-md-4 col-md-offset-0 col-lg-3 tuition-info'>
                        <p class='tuition-header'>5 Monthly Payments</h4>
                        <p class='tuition-price'>$399</p>
                        <p class='tuition-price-note'>+$1000 enrollment fee</p>
                        <a href="#learn-ux-cta" class='page-scroll'><button>Get Started</button></a>
                        <ul class="key-points">
                            <li>Job Preparation</li>
                            <li>Project-based Curriculum</li>
                            <li>1-on-1 Mentorship</li>
                            <li>Project Center Access</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> 
        <!-- End Course Tuition -->
    <section id='home-faqs'>   
        <!-- START FAQs -->
        <div class='row' >
            <!--START: FAQ Pricing -->
            <div class='container'>       
                <div class='faq'>
                    <header>
                        <h3>Frequently Asked Questions</h3>
                    </header>
                  
                    <!--START: FAQ Pricing Left Column -->
                    <div class='col-xs-12 col-sm-6'>
                        <!-- <div class='row'> -->
                            <h4>What is your refund policy?</h4>
                            <p class='faq-topic-answer'>UX Academy's course is results-driven. If you feel as though UX Academy is not the right fit for you after all, you can withdraw from the course at anytime. Withdraw in the first week of your start date, and receive a full refund. If you withdraw later, get a refund based of the number of days you have been in the program, minus a non-refundable cancelation fee of $1000.</p>
                        <!-- </div> -->
                             <h4>What am I going to learn at UX Academy?</h4>
                            <p class='faq-topic-answer'>Our course is comprised of design concepts (UI & UX) for web and mobile applications. The course focuses on user research, usability testing, and learning industry standard tools like Photoshop, Illustrator, Sketch, and InVision. </p>
                            <p class='faq-topic-answer'>If you're looking to focus on user experience and design without digging into code, then our course is for you.</p>

                            <h4>Is there any required prep work?</h4>
                            <p class='faq-topic-answer'>Yes. Once you enroll, we will provide you with prepwork to complete prior to your course start date. The amount of time needed to spend on prep work varies on your experience level. That said, we recommend you give yourself 50-100 hours to spend on prep work before your course start date.</p>

                    </div>
                    <!--END: FAQ Pricing Left Column -->

                    <!--START: FAQ Pricing Right Column -->
                    <div class='col-xs-12 col-sm-6'>
                        <!-- <div class='row'> -->
                        <h4>Do I need to have any design experience to attend UX Academy?</h4>
                        <p class='faq-topic-answer'>No. You do not need any experience to enroll in UX Academy. We have designed our prep work for beginners so you can start with a solid design foundation before your course starts.</p>
                            
                        <!-- </div> -->
                        <!-- <div class='row'> -->
                        <h4>How does the application process work?</h4>
                        <p class='faq-topic-answer'>Once you apply, we will email you within 3 business days to schedule an interview with our Admissions Director. The purpose of the interview is so we can better understand your career goals and answer any questions you may have. </p>

                        <h4>What happens after I apply?</h4>
                        <p class='faq-topic-answer'>Once you apply, a Student Advisor will contact you within 2-3 business days to setup a phone meeting. This meeting is so we can better understand your career goals and your experience level, while providing you with the opportunity to ask any questions you may have.</p>

                        <h4>How does mentoring work exactly?</h4>
                        <p class='faq-topic-answer'>A week before the course starts, you will be able to choose your mentor. Each week you will have weekly meetings with your mentor to provide feedback on assignments, projects, and help you prepare for your first job. </p>

                        <div id="more-questions">
                            <a href="/faq" class='pull-right faq-link'>View All Frequently Asked Questions<i class="fa fa-angle-double-right"></i></a>
                        </div>

                        </div>
                    <!-- </div> -->
                    <!--END: FAQ Pricing Right Column -->
                </div>

            </div>
            <!-- END: FAQ Pricing -->
        </div>
        <!-- END FAQs -->
    </section> 


     <!-- Start CTA Learn UX Design -->
        
          <div class='row' id='learn-ux-cta'>
            <div class="container">
                <a name="apply"></a>
            <h3>Next course start date is on <span>October 19th.</span> Apply Now.</h3>
            <form class='learnUX' method='POST' action='learn-ux-design'>
                <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                    <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                </div>
              
              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-user'></i>
                <input type='text' class='form-control' name='full_name' placeholder='Your Full Name' required>
              </div>            

              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-envelope'></i>
                <input type='email' class='form-control' name='email' placeholder='Email Address' required>
              </div>

              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-phone'></i>
                <input type='tel' class='form-control' name='phone' placeholder='Phone Number' required>
              </div>

              
              <button type='submit' id='learn-ux-submit'>Apply</button>
              

            </form>

            <div class="message"></div>
            <p id='privacy_guarantee'>We guarantee 100% privacy. Your information will not be shared.</p>
          </div>
        </div>
        <!-- End CTA Learn UX Design -->

        <!-- Start Footer -->
        <footer id="footer">
          <div class="container">
            <!-- <div class="container"> -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="footer-nav">
                          <div class="col-xs-6 col-sm-4">
                            <div class="social-icons">
                              <a href="https://www.facebook.com/uxacad" target='_'><i class="fa fa-facebook"></i></a>
                              <a href="https://twitter.com/AcademyUX" target='_'><i class="fa fa-twitter"></i></a>
                              
                            </div>
                          </div>
                             <!-- <ul class="footer-list pull-left">
                              <li> -->
                                   
                              <!-- </li>
                            </ul> -->
                          <div class="col-md-4  visible-md visible-lg">  
                            <span class='copyright visible-sm visible-md visible-lg'>&copy 2015 UX Academy, LLC.  All Rights Reserved.</span>
                          </div>
                          <div class="col-xs-6 col-sm-4">
                              <ul class="footer-list pull-right">
                                  <li>
                                      <span class="questions"> Questions? </span><a href="mailto:someone@example.com">info@uxacademy.io</a>
                                  </li>
                              </ul>
                          </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
            </div>
        </footer>
        <!-- End Footer -->
    </section>





   <!--  <section id='webinar'>
        <div class='container'>
            <div class='row'>
                <div class='col-xs-5 col-xs-offset-1' id='webinar-cta'>
                    <h4>Attend a Course Webinar</h4>
                    <p>Have questions about the course? Join us for a 45 minute 'open house' webinar to get your questions answered and receive more information about the course.</p>
                    <a href="">LEARN MORE</a><i class="fa fa-angle-double-right"></i>
                </div>
                <div class='col-xs-6' id='staff-rollcall'>
                    <div class='col-xs-4 staff-callcard'>
                        <div class='staff-pic' id='staff-left'></div>
                        <p class='staff-name'>Michael Joseph</p>
                        <p class='staff-position'>Course Director</p>
                    </div>
                    <div class='col-xs-4 staff-callcard'>
                        <div class='staff-pic' id='staff-mid'></div>
                        <p class='staff-name'>Melinda Noll</p>
                        <p class='staff-position'>Student Advisor</p>
                    </div>
                    <div class='col-xs-4 staff-callcard'>    
                        <div class='staff-pic' id='staff-right'></div>
                        <p class='staff-name'>Josh Rieland</p>
                        <p class='staff-position'>Program Coordinator</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


       


    
   
   

   


    <!-- jQuery -->
    <script src="/assets/js/jquery.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/assets/js/jquery.easing.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/assets/js/bootstrap.min.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-64642522-1', 'auto');
      ga('send', 'pageview');

    </script>

    <script type="text/javascript">
       $(".timer").countdown("2015/07/30", function(event) {
         $('.days').text(event.strftime('%D'));
         $('.hours').text(event.strftime('%H'));
         $('.minutes').text(event.strftime('%M'));
         $('.seconds').text(event.strftime('%S'));
       });
     </script>


</body>

</html>