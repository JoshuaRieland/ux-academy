<?php require_once('./config.php'); ?>
<!DOCTYPE html>
<html class="full" lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Payments | UX Academy</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,300,600,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>

<body id='inside-pages'>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/img/ux-academy-logo-2.png" alt='ux academy logo'>
                    <!-- <p class='logo-text'><strong>UX</strong>ACADEMY</p> -->
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ">

                    <li>
                        <a href="#learn-ux-cta" class='page-scroll'><strong>Apply Now</strong></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- start hero -->
    <section id="intro">
        <div class="row">
            <div class="course-hero"  id='faq_bg'>
                <div class="container">

                    <!-- hero content section -->
                    <div class="hero-content">
                        <h2 class='hero-heading'>Payments</h2>
                        <h1 class="hero-small">Full Tuition or Monthly Payments</h1>
                    </div>
                    <!-- hero content end -->

                </div>
            </div>
        </div>
      
    </section>
    <!-- end hero -->


    <style type="text/css">

    section#payment-forms{
    	padding:; 25px;
    }	
    	#full-payment, #partial-payment{
    		text-align: center;
    		margin-top: 20px;
    		margin-bottom: 20px;
        }
        #payment-note{
        	text-align: center;
        	padding: 25px;
        }
    </style>
   



    <section id='course-tuition'>
    	<div class='container'>
	    	<div class='row'>
	    		<h4 id='payment-note'>
	    			Please use the email address you registered with. This will be the email that relate your payments with student portal. 
	    		</h4>
	    	</div>
	    </div>
    	 
    	 <div class='row'>
    	 	<div class='container'>

            <div class='col-xs-8 col-xs-offset-2 col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 tuition-info'>
                <p class='tuition-header'>ALL UPFRONT</h4>
                <p class='tuition-price'>$2995</p>
                <p class='tuition-price-note'>One-time payment</p>
                <ul class="key-points">
                    <li>Job Preparation</li>
                    <li>Project-based Curriculum</li>
                    <li>1-on-1 Mentorship</li>
                    <li>Project Center Access</li>
                </ul>
                <form action="charge" method="post" id='full-payment'>
					 <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
					          data-key="<?php echo $stripe['publishable_key']; ?>"
					          data-description="Pay Full Tuition"
					          data-amount="299500"
					          data-locale="auto"
					          data-billing-address='true'
					          data-panel-label='Pay Full Tuition:'>
					</script>
				</form>
            </div>
            <div class='col-xs-8 col-xs-offset-2 col-sm-5 col-sm-offset-0 col-md-4 col-md-offset-0 col-lg-4 tuition-info'>
                <p class='tuition-header'>5 Monthly Payments</h4>
                <p class='tuition-price'>$399</p>
                <p class='tuition-price-note'>+$1000 enrollment fee</p>
                <ul class="key-points">
                    <li>Job Preparation</li>
                    <li>Project-based Curriculum</li>
                    <li>1-on-1 Mentorship</li>
                    <li>Project Center Access</li>
                </ul>
                <form action="charge-split" method="post" id='full-payment'>
					<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
					          data-key="<?php echo $stripe['publishable_key']; ?>"
					          data-description="Pay Monthly Installment"
					          data-amount="39900"
					          data-locale="auto"
					          data-billing-address='true'
					          data-panel-label='Pay Partial Tuition:'
					          >
					</script>
				</form>
            </div>
        </div>
        </div>
    </section>



        <footer id="footer">
          <div class="container">
            <!-- <div class="container"> -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="footer-nav">
                          <div class="col-xs-6 col-sm-4">
                            <div class="social-icons">
                              <a href="https://www.facebook.com/uxacad" target='_'><i class="fa fa-facebook"></i></a>
                              <a href="https://twitter.com/AcademyUX" target'_'><i class="fa fa-twitter"></i></a>
                              
                            </div>
                          </div>
                             <!-- <ul class="footer-list pull-left">
                              <li> -->
                                   
                              <!-- </li>
                            </ul> -->
                          <div class="col-md-4  visible-md visible-lg">  
                            <span class='copyright visible-sm visible-md visible-lg'>&copy 2015 UX Academy, LLC.  All Rights Reserved.</span>
                          </div>
                          <div class="col-xs-6 col-sm-4">
                              <ul class="footer-list pull-right">
                                  <li>
                                      <span class="questions"> Questions? </span><a href="mailto:someone@example.com">info@uxacademy.io</a>
                                  </li>
                              </ul>
                          </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
            </div>
        </footer>
     

     
        <!-- jQuery -->
        <script src="/assets/js/jquery.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/assets/js/jquery.easing.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/assets/js/bootstrap.min.js"></script>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-64642522-1', 'auto');
          ga('send', 'pageview');

        </script>

        <script type="text/javascript">
           $(".timer").countdown("2015/07/30", function(event) {
             $('.days').text(event.strftime('%D'));
             $('.hours').text(event.strftime('%H'));
             $('.minutes').text(event.strftime('%M'));
             $('.seconds').text(event.strftime('%S'));
           });
         </script>

</body>

</html>