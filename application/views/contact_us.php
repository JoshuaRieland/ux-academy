<!DOCTYPE html>
<html class="full" lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Learn to become a UX designer by working through a series of hands on projects while receiving mentorship from experienced designers.">

    <title>Become a UX Designer in 12 weeks | UX Academy</title>

    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="/assets/img/uxa-favicon.png" type="image/png" />
    <!-- Custom CSS -->
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,300,600,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->

<link rel="stylesheet" type="text/css" href="/assets/css/sky-form.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 2000, 'easeInOutExpo');
            event.preventDefault();
        });
    });

    // AJAX Contact-Us Form
    $(document).on('submit', 'form.sky-form', function(){
        $.post(
            $(this).attr('action'),
            $(this).serialize(),
            function(returned_data){                
                $('div.message').append(
                    "<h5 style='text-align: center;'>"+'We have received your message and will address your question or concern as soon as possible.'+"</h5>"
                );
                $('input#name').val('');
                $('input#email').val('');
                $('input#subject').val('');
                $('textarea#message').val('');
                $('input#captcha').val('');

            },
            "json"
        )   
        return false; 
    });

    // AJAX Loading Spinner
    $(document).ready(function(){
    var $loading = $('.loadingDiv').hide();
    $(document)
      .ajaxStart(function () {
        $loading.show();
      })
      .ajaxStop(function () {
        $loading.hide();
      });
    })

</script>

</head>

<body id='inside-pages' data-spy="scroll" data-target=".navbar-static-top">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><img src="/assets/img/ux-academy-logo-2.png" alt='ux academy logo'>
                    <!-- <p class='logo-text'><strong>UX</strong>ACADEMY</p> -->
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ">
                    <li>
                        <a href="#learn-ux-cta" class='page-scroll'><strong>Apply Now</strong></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <section id="intro">
        <div class="hero-contact visible-sm visible-md visible-lg">
            <div class="container">
                <!-- hero content section -->
                <!-- <div class="hero-content">
                    <h2 class='hero-heading'> Design a new career</h2>
                    <h1 class="hero-small"><strong>Become a UX Designer</strong> in 12, 24, or 36 weeks <br /> with our online, project-based course.</h1>
                    <div class="ctas">
                       <form class='stay_informed' action='stay-informed' method='POST'>
                            <label for='informed_email'>Courses Begin Every 6 Weeks</label>
                            <input type='email' name='email' placeholder='Enter Your Email' id='informed_email' class='informed_email' required>
                            <button type='submit' class='informed_btn'>Stay Informed</button>
                        </form> 
                        <div class='informed_message'></div>
                       
                    </div>
                    
                </div> -->
                <!-- hero content end -->
            </div>
        </div>
        <!-- will not upload photo as extra small screen res -->
        <!-- <div class="hero-mobile visible-xs">
            <div class="hero-content">
                <h2 class='hero-heading'> Design a new career</h2>
                    <h1 class="hero-small"><strong>Become a UX Designer</strong> in 12, 24, or 36 weeks <br /> with our online, project-based course.</h1>
                    <div class="ctas">
                       <form class='stay_informed visible-lg visible-md visible-sm' action='/stay-informed' method='POST'>
                            <label for='informed_email'>Courses Begin Every 6 Weeks</label>
                            <input type='email' name='email' placeholder='Enter Your Email' id='informed_email' class='informed_email' required>
                            <button type='submit' class='informed_btn'>Stay Informed</button>
                        </form> 
                         <div class='informed_message'></div>
                    </div>
            </div>
        </div>  -->
    </section>

    <section id='contact-section'>
        <div class="container">
            <div class="row">
                <div class="col-md-8 margin30 sky-form-columns">
                    <h3 class="heading">Contact info</h3>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </p>
                    <div class='message'></div>
                    <form class="sky-form" action="email-us" method="post" id="sky-form">
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        <fieldset>                  
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Name</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" name="name" id="name" required>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">E-mail</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope-o"></i>
                                        <input type="email" name="email" id="email" required>
                                    </label>
                                </section>
                            </div>
                            
                            <section>
                                <label class="label">Subject</label>
                                <label class="input">
                                    <i class="icon-append fa fa-tag"></i>
                                    <input type="text" name="subject" id="subject" required>
                                </label>
                            </section>
                            
                            <section>
                                <label class="label">Message</label>
                                <label class="textarea">
                                    <i class="icon-append fa fa-comment"></i>
                                    <textarea rows="4" name="message" id="message" required></textarea>
                                </label>
                            </section>
                            
                            <section>
                                <label class="label">Enter characters below:</label>
                                <?php echo $image; ?><br><br>
                                <label class="input input-captcha">
                                    <input type="text" maxlength="6" name="captcha" id="captcha" required>
                                </label>
                            </section>
                            
                            <section>
                                <label class="checkbox"><input type="checkbox" name="copy" value='on'><i></i>Send a copy to my e-mail address</label>
                            </section>
                        </fieldset>
                        <div class='loadingDiv' style='width: 50px; height: 50px; margin: 0 auto;  z-index: 9999;'>
                            <i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></i>
                        </div>
                        <footer>
                            <button type="submit" class="btn border-black btn-lg">Send message</button>
                        </footer>
                        
                        <div class="message">
                            <i class="fa fa-check"></i>
                            <p>Your message was successfully sent!</p>
                        </div>
                    </form> 
                   
                </div>
                <div class="col-md-4">
                    <h3 class="heading">Contact info</h3>
                    <ul class="list-unstyled contact contact-info">
                        <li>
                            <p><strong><i class="fa fa-map-marker"></i> Address:</strong> 220 2nd Ave S</p>
                            <p id='address_alignment'>Seattle, WA 98105</p>
                        </li> 
                        <li><p><strong><i class="fa fa-envelope"></i> Mail Us:</strong> <a href="mailto:info@uxacademy.io">info@uxacademy.io</a></p></li>
                    </ul>
                    <div class="divide40"></div>
                    <h4>Get social</h4>
                    <ul class="list-inline social-1">
                        <li><a href="https://www.facebook.com/uxacad" target='_'><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/AcademyUX" target='_'><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!--contact advanced container end-->

<style type="text/css">
    #address_alignment{
        padding-left: 105px;
        margin-top: -10px;
    }
</style>

        <!-- Embedded Google Maps -->
        <div id="map-canvas"></div>

    </section>

    <section id='course-section'>
     <!-- Start CTA Learn UX Design -->
        
          <div class='row' id='learn-ux-cta'>
            <div class="container">
                <a name="apply"></a>
            <h3>Next course start date is on <span>October 19th.</span> Apply Now.</h3>
            <form class='learnUX' method='POST' action='learn-ux-design'>
              
              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-user'></i>
                <input type='text' class='form-control' name='full_name' placeholder='Your Full Name' required>
              </div>            

              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-envelope'></i>
                <input type='email' class='form-control' name='email' placeholder='Email Address' required>
              </div>

              <div class='inner-addon right-addon'>
                <i class='glyphicon glyphicon-phone'></i>
                <input type='tel' class='form-control' name='phone' placeholder='Phone Number' required>
              </div>

              
              <button type='submit' id='learn-ux-submit'>Apply</button>
              

            </form>

            <div class="message"></div>
            <p id='privacy_guarantee'>We guarantee 100% privacy. Your information will not be shared.</p>
          </div>
        </div>
        <!-- End CTA Learn UX Design -->

        <!-- Start Footer -->
        <footer id="footer">
          <div class="container">
            <!-- <div class="container"> -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="footer-nav">
                          <div class="col-xs-6 col-sm-4">
                            <div class="social-icons">
                              <a href="https://www.facebook.com/uxacad" target='_'><i class="fa fa-facebook"></i></a>
                              <a href="https://twitter.com/AcademyUX" target='_'><i class="fa fa-twitter"></i></a>
                              
                            </div>
                          </div>
                             <!-- <ul class="footer-list pull-left">
                              <li> -->
                                   
                              <!-- </li>
                            </ul> -->
                          <div class="col-md-4  visible-md visible-lg">  
                            <span class='copyright visible-sm visible-md visible-lg'>&copy 2015 UX Academy, LLC.  All Rights Reserved.</span>
                          </div>
                          <div class="col-xs-6 col-sm-4">
                              <ul class="footer-list pull-right">
                                  <li>
                                      <span class="questions"> Questions? </span><a href="mailto:someone@example.com">info@uxacademy.io</a>
                                  </li>
                              </ul>
                          </div>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
            </div>
        </footer>
        <!-- End Footer -->
    </section>





   <!--  <section id='webinar'>
        <div class='container'>
            <div class='row'>
                <div class='col-xs-5 col-xs-offset-1' id='webinar-cta'>
                    <h4>Attend a Course Webinar</h4>
                    <p>Have questions about the course? Join us for a 45 minute 'open house' webinar to get your questions answered and receive more information about the course.</p>
                    <a href="">LEARN MORE</a><i class="fa fa-angle-double-right"></i>
                </div>
                <div class='col-xs-6' id='staff-rollcall'>
                    <div class='col-xs-4 staff-callcard'>
                        <div class='staff-pic' id='staff-left'></div>
                        <p class='staff-name'>Michael Joseph</p>
                        <p class='staff-position'>Course Director</p>
                    </div>
                    <div class='col-xs-4 staff-callcard'>
                        <div class='staff-pic' id='staff-mid'></div>
                        <p class='staff-name'>Melinda Noll</p>
                        <p class='staff-position'>Student Advisor</p>
                    </div>
                    <div class='col-xs-4 staff-callcard'>    
                        <div class='staff-pic' id='staff-right'></div>
                        <p class='staff-name'>Josh Rieland</p>
                        <p class='staff-position'>Program Coordinator</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->


       


    
   
   

   


    <!-- jQuery -->
    <script src="/assets/js/jquery.js"></script>

    <!-- Plugin JavaScript -->
    <script src="/assets/js/jquery.easing.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/assets/js/bootstrap.min.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-64642522-1', 'auto');
      ga('send', 'pageview');

    </script>

    <!-- // <script type="text/javascript"> -->
    <!-- //    $(".timer").countdown("2015/07/30", function(event) { -->
    <!-- //      $('.days').text(event.strftime('%D')); -->
    <!-- //      $('.hours').text(event.strftime('%H')); -->
    <!-- //      $('.minutes').text(event.strftime('%M')); -->
    <!-- //      $('.seconds').text(event.strftime('%S')); -->
    <!-- //    }); -->
    <!-- //  </script> -->

    <!--gmap js-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript">
        var myLatlng;
        var map;
        var marker;

        function initialize() {
            myLatlng = new google.maps.LatLng(47.600656, -122.331351);

            var mapOptions = {
                zoom: 13,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                draggable: true
            };
            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            var contentString = '<p style="line-height: 20px;"><strong>assan Template</strong></p><p>Vailshali, assan City, 302012</p>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Marker'
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    

</body>

</html>
